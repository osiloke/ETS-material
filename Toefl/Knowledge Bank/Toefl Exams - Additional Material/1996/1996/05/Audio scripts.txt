1996��5�� ��������

1. Would you like some milk in your coffee?
Please.
What does the woman mean?

2. Could you help bother me plan a surprise party for Meg?
Sure. What can I do?
What does the woman mean?

3. Let's jog for another mile.
I'll try. But I'm running out of steam.
What does the man mean?

4. Should we call Marsha and tell her about the meeting?
I'm not sure. It's up to you.
What does the man imply?

5. I can't seem to solve this problem.
Neither can I.
What does the woman say about the problem?

6. Hello. I'm calling to see of the summer position you advertised in the paper. Is it still available?
Uh, yes. Certainly. When could you come to the office for an interview?
What will the man probably do?

7. It took me five days to drive down to Florida.
Five days? I could've walked there in less time.
What does the woman mean?

8. Excuse me, did anybody find a black umbrella after the last show? I left it under my chair.
As a matter of fact, we did. Check it at the ticket counter. That's where we turn it the lost-and-found items.
What does the man suggest the woman do?

9. I spent the whole weekend totally absorbed in this biography.
And you still haven't finished reading it?
What can be inferred about the biography?

10. You haven't phoned Harry yet, have you?
As a matter of fact, I have.
What does the man mean?

11. I can't seem to find my photo album.
   I think Mary is looking at it in the living room.
   What does the man mean?

12. Did you hear that Mitchell turned down that job?
   Yeah. The hours were convenient, but she wouldn't have been able to make ends meet.
   What does the man say about Mitchell?

13. Is there a bus I can get to the station?
   There is. But you can't rely on it. I can give you a ride if you can wait while I put these things   
   away.
   What is the woman going to do?

14. I hate memorizing vocabulary.
Well, it's part of learning a second language so there not whole at all you can do about it.
What does the woman tell the man?

15. Have you had a chance to wear your new shirt yet?
That reminds me. I've been meaning to exchange it to a larger size.
What does the man imply about the shirt?

16. Did you tell Carl that the concert starts at eight?
I've tried several times, but the line's been busy.
What does the man mean?

17. I had the brown paper and the string. Could you hand me the tape and the scissors please?
Sure. Here they are. But remember all this has to be weighted before it goes to.
What is the man probably doing?

18. Andrew likes his new place. But he is not too happy about all the noise.
What did he expect? He is right next to the airport.
What does the woman mean?

19. I don't remember exactly what the lab hours are. But they are posted on the door.
I just checked that schedule and it says that the lab opens at ten. But it's all locked up.
What can be inferred about the lab?

20. The subway is running behind schedule, and traffic is backed up for blocks. I don't know if we'll make the 7:15 show.
It's a beautiful night. Let's try to get there on foot. And if we don't make it. Let's just have dinner near the theater.
What does the woman suggest they do?

21. We've been working on this proposal for so long that my eyes are starting to blur.
Why don't we get out of here? We can wrap it up later.
What does the woman mean?.

22. That was really an interesting piece of music. How did you find out about it?
They played it in the classical station last week.
What does the man say about the music?

23. I spent the whole weekend working on that chemistry assignment.
Don't tell me. I have to do the whole thing tonight.
What does the woman imply?

24. Come to the movies with us. Everyone needs to take a break once in a while.
I guess I might as well. I've been studying so long I can hardly concentrate.
What does the man mean?

25. Pete's really out of it these days.
Yeah. I know. Ever since he met Ann, he's been in another world.
What does the woman imply about Pete?

26. Look at all those cars lined up for the ferry. There must be forty ahead of us.
Yeah. I think it's going to be a while.
What does the woman imply?

27. I thought Pam said the math test wasn't until Monday.
Ellen, you should know better than to take Pam's words for anything.
What does the man imply about Pam?

28. So far the clubs are about three hundred dollars in the red, and we still have four months to to before membership renewal.
Well, we may have to raise our dues. 
What does the man suggest they do?

29. I hear they hire two more students to work in the mall room.
They are just a little short of a full staff man.
What does the woman mean?

30. Fred is off to the golf course again.
You would think he was practicing for the championship.
What does the woman imply about Fred?

PART B
31-34  Two friends discussing weekend plans.
*Susan, I could really use your help this weekend. 
*What is it, John? Another term paper?
*No, no. This is easy compared to that. My cousin is coming on Thursday. She has an interview at the college and I promised my aunt I look after her. We are going to the game on Friday, but Saturday I'm on duty at the library all day and can't get out of it. Uh, I was wondering if you could show her around during the day and maybe we can all meet for dinner later.
*Sure. I don't have any plans. What kind of things does she like to do?
*Actually I haven't seen her for three years. She lives so far away. But this will be her first time on a college campus, she is still in high school. So she probably enjoy anything on campus.
*Well, there is a music festival in the auditorium. That's a possibility. Only I hope it doesn't show. They are predicting 68 inches for the weekend. Everything will be closed down then.
*Well, how about for the time being. I'll plan on dropping her off at your place on the way to work, around eleven. But if there is a blizzard, I'll give you a call and see if we can figure something else out.
*Sounds good. Meantime I'll keep Saturday open. We can touch base Friday night when we have a better idea of the forecast.
*I hope this works out. I feel kind of responsible. She won't know a way around. And I want her to have a good time. Anyway I really appreciate your help. I owe you one.
*No problem I'll talk to you tomorrow.

31. What does John ask Susan to do?
32. What will John do on Saturday?
33. What does John say about his cousin's interests?
34. What can be inferred about John's cousin?

35-37  A conversation in a university cafeteria.
*I see you are having the fish for lunch.
*That's right. Fish sticks and tomato soup, my favorites.
*I bet they were frozen.
*What?
*The fish sticks.
*Of course they were frozen. We are hundreds of miles from the ocean. The cafeteria can't afford to fly in fresh fish.
*I just mentioned it because an anecdote Professor Chambers told in class this morning.
*Which class?
*My American social history. It's a lot of fun.
*He talked about fish sticks?
*Not exactly. Bur he did talk about frozen fish. OK, this is back in 1920, right? It's 20 degrees below zero. And this guy Clarence Birdseye's out ice-fishing.
*Where is this, Antarctica?
*No. Massachusetts I think. Anyway he catches a fish and drops it beside him on the ice and it freezes solid.
*So?
*So, later at home he thaws out the fish in a bucket of water and it's alive. Of course Birdseye is amazed. But he eventually figures it out that the fish froze so fast that no large ice crystals fomed.
*What do you mean?
*Look, usually when a plant or animal cell freezes, large ice crystals form inside and eventually tear the cell walls. That's what kills the frozen plant or animal. And also what changes the taste of something that's been frozen. But if you freeze something quickly, only very small ice crystals form. So Birdseye invented the process of quick freezing food.
*And that was the beginning of the frozen foods?
*Right.

35. Why does the woman tell the man the story?
36. Why is it important to quick freeze fresh food?
37. What was Clarence Birdseye surprised to learn?

PART C
38 to 41 A lecture about the railroad industry.
At the beginning of the century the railroads were used to haul everything. Powerful railroad barons made fortunes without laving to be accountable to the public or considerable to the customers. But cars and trucks changed all of that. And by 1970, the rail industry was beset with problems. Trucks were taking all the new business. And even so the rail industry remained indifferent to customers. Also many regulations kept the rail industry from adjusting to shifting market. But in 1980, the rail industry entered the modern era when a deregulation bill was passed that allowed railroad companies to make quick adjustments to fees and practices. Companies reduced their lines by 1/3 and used fewer employees. They also took steps to minimize damage to product. And to increase their shipping capacity by stacking freight containers on railroad cars. To accommodate these taller loads, underpasses and tunnels were enlarged. The image of the rail industry has changed dramatically. Today companies are very responsive to customers and are gaining increasing market shares in the shipping industry. The railroad safety record is also strong. Freight trains have an accident rate that is only 1/3 that of the truck industry. Trains also come out ahead of the trucks on environmental grounds because they give off only 1/10 to 1/3 the pollution that is emitted by trucks. And railroading does not were out highways as trucks do.

38. What does the speaker mainly discuss?  
39. What development caused a decline in the use of railroads?
40. What is one reason why the railroad industry is gaining public support?
41. According to the speaker, what expense does trucking public support?

42 to 45 Talk in an introductory biology class.
This coffee can contains soil from my garden. And I prepared slide samples to show you that it is alive. This ordinary backyard dirt is crawling with microbes. Microbes is not a very specific term. There are hundreds of thousands of different species called microbes. What they have to common is that we can't see them with a naked eye. They are microspopic. Look at this slide. You should see some round cells. Those are yeasts. Yeasts are fermenters. And they are necessary for making bread, beer, yogurt and so forth. Now look for an irregular shape with hairs coming out of it. That's a mold. Molds are decomposers, and they are responsible for the decomposition of my compile teeth. You should also see some protozoa. Some protozoa, like the one causes malaria are harmful to people. A microbic causes disease is called a pathogen. Finally you should see a lot of squiggly lines. They are bacteria. The oldest form of life on earth. Like the other microbes, bacteria are single-cells do not have a nuclear. So I've got about a teaspoon of soil in my hand here. In that teaspoon are about ten thousand protozoa, 200 thousand mold cells, a million yeast, probably a billion bacteria.

42. What is the main purpose of the talk?
43. Why did the speaker take a soil sample from his garden?
44. What does the speaker imply about yeasts?
45. What does the speaker ask the class to look at during the talk?


